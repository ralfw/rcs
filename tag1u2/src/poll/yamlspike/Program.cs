﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;

namespace yamlspike
{
    class Program
    {
        static void Main(string[] args)
        {
            var fb = new Fragebogen
            {
                Fragen = new[]
                {
                    new Frage
                    {
                        Text = "f1",
                        Nummer = "1",
                        Antwortmöglichkeiten = new[]
                        {
                            new Antwortmöglichkeit
                            {
                                Nummer = "1.1",
                                Text = "am11",
                                Markiert = false,
                                Antwort = false
                            }
                        }
                    }
                }
            };


            var p = new Person {Name="peter"};

            var serializer = new Serializer();

            var sb = new StringBuilder();
            var sw = new StringWriter(sb);
            serializer.Serialize(sw, fb);

            //Console.WriteLine(sb);

            var deserializer = new Deserializer();
            var result = deserializer.Deserialize<Fragebogen>(new StringReader(sb.ToString()));

            Console.WriteLine(result.Fragen.First().Text);
        }
    }

    public class Person
    {
        public string Name { get; set; }
    }
}
