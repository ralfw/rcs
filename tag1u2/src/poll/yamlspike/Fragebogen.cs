using System;
using System.Collections.Generic;
using System.Linq;

namespace yamlspike
{
	public class Fragebogen {
		public IEnumerable<Frage> Fragen { get; set; }

		public void Antworten_markieren(IEnumerable<string> antwortnummern) {
			this.Fragen
				.SelectMany (f => f.Antwortmöglichkeiten)
				.Where (am => antwortnummern.Any (an => an == am.Nummer))
				.ToList ()
				.ForEach (am => am.Markiert = true);
		}
	}

	public class Frage {
		public string Nummer { get; set; }
        public string Text { get; set; }
        public IEnumerable<Antwortmöglichkeit> Antwortmöglichkeiten { get; set; }
	}

	public class Antwortmöglichkeit {
        public string Nummer { get; set; }
        public string Text { get; set; }
        public bool Markiert { get; set; }
        public bool Antwort { get; set; }
	}
}