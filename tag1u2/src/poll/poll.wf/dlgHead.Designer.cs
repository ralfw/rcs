﻿namespace poll.wf
{
    partial class dlgHead
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtFragebogendateipfad = new System.Windows.Forms.TextBox();
            this.txtPrüflingEmail = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnStarten = new System.Windows.Forms.Button();
            this.lstFragebogen = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Fragebogendatei";
            // 
            // txtFragebogendateipfad
            // 
            this.txtFragebogendateipfad.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFragebogendateipfad.Location = new System.Drawing.Point(113, 9);
            this.txtFragebogendateipfad.Name = "txtFragebogendateipfad";
            this.txtFragebogendateipfad.Size = new System.Drawing.Size(213, 21);
            this.txtFragebogendateipfad.TabIndex = 1;
            this.txtFragebogendateipfad.Text = "fragebogen.txt";
            // 
            // txtPrüflingEmail
            // 
            this.txtPrüflingEmail.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPrüflingEmail.Location = new System.Drawing.Point(113, 32);
            this.txtPrüflingEmail.Name = "txtPrüflingEmail";
            this.txtPrüflingEmail.Size = new System.Drawing.Size(100, 21);
            this.txtPrüflingEmail.TabIndex = 3;
            this.txtPrüflingEmail.Text = "test@web.de";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(20, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Email";
            // 
            // btnStarten
            // 
            this.btnStarten.Location = new System.Drawing.Point(113, 59);
            this.btnStarten.Name = "btnStarten";
            this.btnStarten.Size = new System.Drawing.Size(100, 23);
            this.btnStarten.TabIndex = 5;
            this.btnStarten.Text = "Starten";
            this.btnStarten.UseVisualStyleBackColor = true;
            this.btnStarten.Click += new System.EventHandler(this.btnStarten_Click);
            // 
            // lstFragebogen
            // 
            this.lstFragebogen.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstFragebogen.FormattingEnabled = true;
            this.lstFragebogen.ItemHeight = 15;
            this.lstFragebogen.Location = new System.Drawing.Point(23, 88);
            this.lstFragebogen.Name = "lstFragebogen";
            this.lstFragebogen.Size = new System.Drawing.Size(303, 319);
            this.lstFragebogen.TabIndex = 6;
            this.lstFragebogen.Click += new System.EventHandler(this.lstFragebogen_Click);
            // 
            // dlgHead
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(351, 435);
            this.Controls.Add(this.lstFragebogen);
            this.Controls.Add(this.btnStarten);
            this.Controls.Add(this.txtPrüflingEmail);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtFragebogendateipfad);
            this.Controls.Add(this.label1);
            this.Name = "dlgHead";
            this.Text = "Poll";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtFragebogendateipfad;
        private System.Windows.Forms.TextBox txtPrüflingEmail;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnStarten;
        private System.Windows.Forms.ListBox lstFragebogen;
    }
}

