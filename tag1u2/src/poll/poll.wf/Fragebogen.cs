using System;
using System.Collections.Generic;
using System.Linq;

namespace poll.wf
{
	public class Fragebogen {
		public IEnumerable<Frage> Fragen { get; set; }
	}

	public class Frage {
		public string Nummer { get; set; }
		public string Text { get; set; }
		public IEnumerable<Antwortmöglichkeit> Antwortmöglichkeiten { get; set; }
	}

	public class Antwortmöglichkeit {
		public string Nummer { get; set; }
		public string Text { get; set; }
		public bool Markiert { get; set; }
		public bool Antwort { get; set; }
	}
}