﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;

namespace poll.wf
{
    public class BodyProvider
    {
        private readonly ConsoleServiceProvider _cliProvider;

        public BodyProvider(ConsoleServiceProvider cliProvider)
        {
            _cliProvider = cliProvider;
        }


        public Fragebogen Starten(string fragebogendateipfad, string prüflingemail)
        {
            var response =_cliProvider.Process(string.Format("starten -f:\"{0}\" -p:\"{1}\" -yaml", fragebogendateipfad, prüflingemail), "");
            return Fragebogen_deserialisieren(response);
        }

        public Fragebogen Antworten(string antwortnummer)
        {
            var response = _cliProvider.Process(string.Format("antworten -a:{0} -yaml", antwortnummer), "");
            return Fragebogen_deserialisieren(response);
        }


        private Fragebogen Fragebogen_deserialisieren(string yaml)
        {
            var deserializer = new Deserializer();
            return deserializer.Deserialize<Fragebogen>(new StringReader(yaml));
        }
    }
}
