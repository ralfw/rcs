﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace poll.wf
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            var cli = new ConsoleServiceProvider("poll.cli.exe");
            var body = new BodyProvider(cli);
            var dlg = new dlgHead(body);

            Application.Run(dlg);
        }
    }
}
