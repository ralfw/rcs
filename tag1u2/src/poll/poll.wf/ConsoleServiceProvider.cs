﻿using System.Text;
using System.Windows.Forms;

namespace poll.wf
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;


    public class ConsoleServiceProvider : IDisposable
    {
        private readonly string serviceFilepath;
        private readonly string commandlineParams;

        public ConsoleServiceProvider(string serviceFilepath) : this(serviceFilepath, "") { }
        public ConsoleServiceProvider(string serviceFilepath, string commandlineParams)
        {
            this.commandlineParams = commandlineParams;
            this.serviceFilepath = serviceFilepath;
        }


        public string Process(string input) { return Process(this.commandlineParams, input); }
        public string Process(string commandlineParams, string input)
        {
            var p = new Process
            {
                StartInfo = new ProcessStartInfo(this.serviceFilepath, commandlineParams)
                {
                    UseShellExecute = false,
                    RedirectStandardInput = true,
                    RedirectStandardOutput = true,
                    CreateNoWindow = true,
                }
            };
            
            p.Start();
            p.StandardInput.Write(input);
            p.StandardInput.Close();

            var output = new List<string>();
            p.OutputDataReceived += (sender, e) => output.Add(e.Data);
            p.BeginOutputReadLine();

            p.WaitForExit();

            return string.Join("\n", output);
        }


        #region IDisposable implementation
        public void Dispose() { }
        #endregion
    }
}
