﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace poll.wf
{
    public partial class dlgHead : Form
    {
        private readonly BodyProvider body;
        private readonly Dictionary<int,string> antwortnummern = new Dictionary<int, string>(); 


        public dlgHead(BodyProvider body)
        {
            this.body = body;
            InitializeComponent();
        }


        private void btnStarten_Click(object sender, EventArgs e)
        {
            var fb = this.body.Starten(this.txtFragebogendateipfad.Text, this.txtPrüflingEmail.Text);
            Fragebogen_anzeigen(fb);
        }


        private void lstFragebogen_Click(object sender, EventArgs e)
        {
            string antwortnummer;
            if (this.antwortnummern.TryGetValue(this.lstFragebogen.SelectedIndex, out antwortnummer)) {
                var fb = this.body.Antworten(antwortnummer);
                Fragebogen_anzeigen(fb);
            }
        }

        private void Fragebogen_anzeigen(Fragebogen fb)
        {
            this.antwortnummern.Clear();
            this.lstFragebogen.Items.Clear();

            foreach (var f in fb.Fragen)
            {
                this.lstFragebogen.Items.Add(string.Format("{0} {1}", f.Nummer, f.Text));
                foreach (var am in f.Antwortmöglichkeiten)
                {
                    this.lstFragebogen.Items.Add(string.Format("  {0} {1} {2}",
                                                 am.Markiert ? "[X]" : "   ",
                                                 am.Nummer,
                                                 am.Text));

                    this.antwortnummern.Add(this.lstFragebogen.Items.Count-1, am.Nummer);
                }
            }
        }
    }
}
