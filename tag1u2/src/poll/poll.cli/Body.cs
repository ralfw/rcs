using System;
using System.Collections.Generic;
using CLAP;
using System.IO;

namespace poll.cli
{
	public class Body {
		public Fragebogen Starten(string fragebogendateipfad, string prüflingemail) {
			var fbprovider = new Fragebogenprovider ();
			var pbprovider = new Prüfungsbogenprovider ();

			var fb = fbprovider.Laden (fragebogendateipfad);
			Prüfung.Fragen_nummerieren (fb);

			var pb = new Prüfungsbogen{ 
				Fragebogendateipfad = fragebogendateipfad,
				Prüflingemail = prüflingemail,
				Antworten = new List<string>()
			};
			pbprovider.Speichern(pb);

			return fb;
		}


		public Fragebogen Antworten(string antwortnummer) {
			var fbprovider = new Fragebogenprovider ();
			var pbprovider = new Prüfungsbogenprovider ();

			var pb = pbprovider.Laden ();

			var fb = fbprovider.Laden (pb.Fragebogendateipfad);
			Prüfung.Fragen_nummerieren (fb);

			Prüfung.Antwort_registrieren (pb, antwortnummer, fb);

			fb.Antworten_markieren (pb.Antworten);

			pbprovider.Speichern (pb);

			return fb;
		}
	}
}