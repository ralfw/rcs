using System;
using System.Collections.Generic;
using CLAP;
using System.IO;

namespace poll.cli
{

	public class Prüfungsbogen {
		public string Fragebogendateipfad;
		public string Prüflingemail;
		public IEnumerable<string> Antworten;
	}
}
