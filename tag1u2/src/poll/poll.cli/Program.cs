﻿using System;
using System.Collections.Generic;
using CLAP;

namespace poll.cli
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			var body = new Body ();
			var head = new Head (body);

			head.Run (args);
		}
	}
}