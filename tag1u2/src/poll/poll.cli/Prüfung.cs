using System;
using System.Collections.Generic;
using CLAP;
using System.Linq;

namespace poll.cli
{
	public class Prüfung {
		public static void Fragen_nummerieren(Fragebogen fragebogen) {
			var fnr = 0;
			foreach (var f in fragebogen.Fragen) {
				f.Nummer = string.Format ("{0}", ++fnr);;
				var anr = 0;
				foreach (var am in f.Antwortmöglichkeiten) {
					am.Nummer = string.Format ("{0}.{1}", f.Nummer, ++anr);
				}
			}
		}


		public static void Antwort_registrieren(Prüfungsbogen prüfungsbogen, string antwortnummer, Fragebogen fragebogen) {
			var dieseFrage = fragebogen.Fragen
								  	   .First(f => f.Antwortmöglichkeiten.Any(am => am.Nummer == antwortnummer));

			var antwortenAndererFragen = prüfungsbogen.Antworten
													  .Where (an => !dieseFrage.Antwortmöglichkeiten.Any(am => am.Nummer == an));

			prüfungsbogen.Antworten = antwortenAndererFragen.Concat(new[]{antwortnummer});;
		}
	}
}