using System;
using System.Collections.Generic;
using System.Text;
using CLAP;
using YamlDotNet.Serialization;

namespace poll.cli
{
	public class Head {
		readonly Body body;

		public Head(Body body) {
			this.body = body;
		}

		public void Run(string[] args) {

			Parser.Run (args, this);
		}


		[Verb]
		void Starten(
			[Required, Aliases("d,f,pfad")] 	         string fragebogendateipfad, 
			[Aliases("e,email,p"), DefaultValue("")]     string prüflingemail,
			[Aliases("y,yaml,yml"), DefaultValue(false)] bool yamlOutput) {

			var fb = this.body.Starten (fragebogendateipfad, prüflingemail);

			if (yamlOutput)
				Fragebogen_ausgeben_yaml(fb);
			else
			{
				Console.WriteLine("Prüfung gestartet: {0}", fragebogendateipfad);
				Fragebogen_ausgeben(fb);
			}
		}

		private void Fragebogen_ausgeben(Fragebogen fragebogen) {
			foreach (var f in fragebogen.Fragen) {
				Console.WriteLine ("{0} {1}", f.Nummer, f.Text);
				foreach (var am in f.Antwortmöglichkeiten) {
					Console.WriteLine ("  {2}{0} {1}", 
									   am.Nummer, 
									   am.Text,
									   am.Markiert ? "[X]" : "   ");
				}
			}
		}

		private void Fragebogen_ausgeben_yaml(Fragebogen fragebogen)
		{
		    Console.OutputEncoding = Encoding.Default;

			var serializer = new Serializer();
			serializer.Serialize(Console.Out, fragebogen);
		}


		[Verb]
		void Antworten(
			 [Required, Aliases("a,n,an,nummer")]         string antwortnummer,
			 [Aliases("y,yaml,yml"), DefaultValue(false)] bool yamlOutput)
		{
			var fb = this.body.Antworten (antwortnummer);

			if (yamlOutput)
				Fragebogen_ausgeben_yaml(fb);
			else
			{
				Console.WriteLine("Antwort registriert: {0}", antwortnummer);
				Fragebogen_ausgeben(fb);
			}
		}
	}
}
