using System;
using System.Collections.Generic;
using System.IO;

namespace poll.cli
{
	public class Fragebogenprovider {
		public Fragebogen Laden(string dateipfad) {
			var fragen = new List<Frage> ();

			using (var sr = new StreamReader (dateipfad)) {
				Frage frage = null;
				Action frage_registrieren = () => {
					if (frage != null) {
						((List<Antwortmöglichkeit>)frage.Antwortmöglichkeiten).Add (
							new Antwortmöglichkeit{Text = "k.A."}
						); 
						fragen.Add(frage);
					}
				};

				string line;
				while ((line = sr.ReadLine ()) != null) {
					line = line.Trim ();
					if (string.IsNullOrEmpty (line)) continue;

					if (line.EndsWith("?")) {
						frage_registrieren ();
						frage = new Frage{Text = line, Antwortmöglichkeiten = new List<Antwortmöglichkeit>()};
					}
					else{
						var istAntwort = false;
						if (line.StartsWith ("*")) {
							istAntwort = true;
							line = line.Substring (1);
						}

						((List<Antwortmöglichkeit>)frage.Antwortmöglichkeiten).Add (
							new Antwortmöglichkeit{Text = line, Antwort = istAntwort}
						); 
					}
				}
				frage_registrieren ();
			}

			return new Fragebogen{ Fragen = fragen };
		}
	}
}