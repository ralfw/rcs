using System;
using System.Collections.Generic;
using CLAP;
using System.IO;
using System.Linq;

namespace poll.cli
{
	public class Prüfungsbogenprovider {
		const string DATEINAME = "prüfungsbogen.txt";


		public Prüfungsbogen Laden() {
			var pb = new Prüfungsbogen ();

			var lines = File.ReadAllLines (DATEINAME);
			pb.Fragebogendateipfad = lines [0];
			pb.Prüflingemail = lines [1];
			pb.Antworten = lines.Skip (2);

			return pb;
		}


		public void Speichern(Prüfungsbogen prüfungsbogen) {
			File.WriteAllLines (DATEINAME, new[]{ 
					prüfungsbogen.Fragebogendateipfad, 
					prüfungsbogen.Prüflingemail,
				}.Concat(prüfungsbogen.Antworten)
			);
		}
	}
}