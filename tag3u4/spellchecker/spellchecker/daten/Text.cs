using System;
using System.Text.RegularExpressions;
using System.Linq;
using CLAP;
using System.Collections.Generic;

namespace spellchecker.daten
{
	class Text {
		public Text(string[] zeilen) {
			this.Zeilen = zeilen;
		}

		public IEnumerable<string> Zeilen { get; private set; }

		public IEnumerable<string> Zeile_im_Kontext(int zeilennummer) {
			if (zeilennummer < 0 || zeilennummer >= this.Zeilen.Count()) return new string[0];
			return this.Zeilen.Skip (zeilennummer-1).Take (zeilennummer > 0 ? 3 : 2);
		}
	}
}