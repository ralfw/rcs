using System;
using CLAP;
using System.Collections.Generic;

namespace spellchecker.daten.dto
{
	class Fehlerkontext {
		public IEnumerable<string> Zeilen;
		public IEnumerable<string> FehlerhafteWorte;
	}	
}