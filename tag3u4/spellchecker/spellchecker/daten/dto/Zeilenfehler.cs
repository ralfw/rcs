using System;
using System.Text.RegularExpressions;
using System.Linq;
using CLAP;
using System.Collections.Generic;

namespace spellchecker.daten.dto
{
	class Zeilenfehler {
		public Zeilenfehler(int zeilennummer, IEnumerable<string> fehlerworte) {
			this.Zeilennummer = zeilennummer;
			this.Fehlerworte = fehlerworte.ToArray ();
		}
			
		public void Löschen(string wort) {
			this.Fehlerworte = this.Fehlerworte.Where (w => w != wort).ToArray ();
		}
			
		public int Zeilennummer { get; private set; }
		public string[] Fehlerworte{ get; private set; }
	}
}