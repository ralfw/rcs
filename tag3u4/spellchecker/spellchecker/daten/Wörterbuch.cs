using System;
using System.Text.RegularExpressions;
using System.Linq;
using CLAP;
using System.Collections.Generic;
using spellchecker.daten.dto;

namespace spellchecker.daten
{
	class Wörterbuch {
		private HashSet<string> index;

		public Wörterbuch(IEnumerable<string> wörter) {
			this.index = new HashSet<string> ();
			foreach (var w in wörter) {
				if (string.IsNullOrWhiteSpace (w)) continue;

				this.index.Add (w.ToLower());
			}
		}

		public IEnumerable<Fehler> Prüfen(IEnumerable<Wort> wörter) {
			foreach (var w in wörter) {
				if (!this.index.Contains(w.Text.ToLower()))
					yield return new Fehler{Text = w.Text, Zeilennummer = w.Zeilennummer};
			}
		}


		public void Wort_lernen(string wort, Action<string> wortGelernt) {
			if (string.IsNullOrWhiteSpace (wort)) return;
			wort = wort.Trim ().ToLower ();
			this.index.Add (wort);
			wortGelernt (wort);
		}
	}
}