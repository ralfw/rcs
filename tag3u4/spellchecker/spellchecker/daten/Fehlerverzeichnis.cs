using System;
using System.Text.RegularExpressions;
using System.Linq;
using CLAP;
using System.Collections.Generic;
using spellchecker.daten.dto;


namespace spellchecker.daten
{
	class Fehlerverzeichnis : IEnumerator<Zeilenfehler> {
		private List<Zeilenfehler> fehler_pro_zeile = new List<Zeilenfehler> ();

		public Fehlerverzeichnis(IEnumerable<Fehler> fehlerliste) {
			this.fehler_pro_zeile = new List<Zeilenfehler> (
				fehlerliste.GroupBy (f => f.Zeilennummer)
				.Select (fg => new Zeilenfehler (
					fg.Key,
					fg.Select (f => f.Text).ToArray ()
				)
				));
			this.Reset ();
		}


		public void Wort_löschen(string wort) {
			var zeilen_mit_wort = this.fehler_pro_zeile.Where (zf => zf.Fehlerworte.Contains (wort));
			zeilen_mit_wort.ToList ().ForEach (zf => zf.Löschen (wort));
		}


		#region IEnumerator implementation
		private int currentIndex;

		public bool MoveNext ()
		{
			this.currentIndex++;
			return this.currentIndex < this.fehler_pro_zeile.Count;
		}

		public void Reset ()
		{
			this.currentIndex = 0;
		}

		object System.Collections.IEnumerator.Current {
			get {
				if (this.currentIndex >= 0 && this.currentIndex < this.fehler_pro_zeile.Count)
					return this.fehler_pro_zeile [this.currentIndex];
				else
					return new Zeilenfehler(-1, new string[0]);
			}
		}
		#endregion

		#region IEnumerator implementation
		Zeilenfehler System.Collections.Generic.IEnumerator<Zeilenfehler>.Current {
			get {
				return ((System.Collections.IEnumerator)this).Current as Zeilenfehler;
			}
		}
		#endregion

		#region IDisposable implementation
		public void Dispose () {}
		#endregion
	}
}