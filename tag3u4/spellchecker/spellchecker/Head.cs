using System;
using CLAP;
using System.Collections.Generic;
using spellchecker.daten.dto;

namespace spellchecker
{
	class Head {
		Body body;

		public Head(Body body) {
			this.body = body;
		}

		public void Run(string[] args) {
			CLAP.Parser.Run (args, this);
		}

		[Verb(IsDefault=true)]
		public void Run(
			[Required, Aliases("t,text")] string textdateipfad, 
			[Required, Aliases("w,wörterbuch,d,dict")] string wörterbuchdateipfad) 
		{
			this.body.Prüfen (textdateipfad, wörterbuchdateipfad);
			var fehlerktx = this.body.Aktueller_Fehlerkontext ();
			Fehler_anzeigen (fehlerktx);
			Menüauswahl (
				() => {
					fehlerktx = this.body.Nächster_Fehlerkontext();
					Fehler_anzeigen(fehlerktx);
				},
				fehlerindex => {
					Fehlerwort_heraussuchen(fehlerindex,
						fehlerwort => this.body.Wort_lernen(fehlerwort));
					fehlerktx = this.body.Aktueller_Fehlerkontext();
					Fehler_anzeigen(fehlerktx);
				});
		}


		private List<string> fehlerworte;

		private void Fehlerwort_heraussuchen(int index, Action<string> fehlerwort_gefunden) {
			if (index < 0 || index >= this.fehlerworte.Count) return;
			fehlerwort_gefunden (this.fehlerworte [index]);
		}


		private void Fehler_anzeigen(Fehlerkontext ktx) {
			foreach (var z in ktx.Zeilen)
				Console.WriteLine (z);
			var i = 0;
			foreach (var f in ktx.FehlerhafteWorte)
				Console.WriteLine ("  {0}: {1}", ++i, f);
			this.fehlerworte = new List<string> (ktx.FehlerhafteWorte);
		}


		private void Menüauswahl(Action weiterblätternGewünscht, Action<int> fehlerindexEingegeben) {
			while (true) {
				Console.Write ("$ ENTER=weiter, Fehlerindex=Wort lernen: ");
				var cmd = Console.ReadLine ();
				if (string.IsNullOrWhiteSpace (cmd))
					weiterblätternGewünscht ();
				else {
					var fehlerindex = 0;
					if (int.TryParse(cmd, out fehlerindex)) {
						fehlerindexEingegeben(fehlerindex-1);
					}
				}
			}
		}
	}
}