using System;
using System.Text.RegularExpressions;
using System.Linq;
using CLAP;
using System.Collections.Generic;
using spellchecker.daten;
using spellchecker.daten.dto;
using spellchecker.provider;


namespace spellchecker
{
	class Body {
		private WörterbuchProvider wbprovider;
		private Text text;
		private Wörterbuch wb;
		private IEnumerator<Zeilenfehler> fehlerverzeichnis;

		public void Prüfen(string textdateipfad, string wörterbuchdateipfad) {
			var textprovider = new TextProvider ();
			this.wbprovider = new WörterbuchProvider (wörterbuchdateipfad);

			this.text = textprovider.Text_laden (textdateipfad);
			this.wb = wbprovider.Wörterbuch_laden ();

			var wörter = spellchecker.domain.Parser.Parse (this.text.Zeilen);
			var fehlerliste = wb.Prüfen (wörter);

			this.fehlerverzeichnis = new Fehlerverzeichnis (fehlerliste);
		}


		public Fehlerkontext Aktueller_Fehlerkontext() {
			var zeilenfehler = this.fehlerverzeichnis.Current;

			var fehlerzeilen = this.text.Zeile_im_Kontext (zeilenfehler.Zeilennummer);

			return new Fehlerkontext{ 
				Zeilen = fehlerzeilen,
				FehlerhafteWorte = zeilenfehler.Fehlerworte
			};
		}


		public Fehlerkontext Nächster_Fehlerkontext() {
			this.fehlerverzeichnis.MoveNext ();
			return this.Aktueller_Fehlerkontext ();
		}


		public void Wort_lernen(string wort) {
			this.wb.Wort_lernen (wort,
				this.wbprovider.Wort_lernen);
			((Fehlerverzeichnis)this.fehlerverzeichnis).Wort_löschen (wort);
		}
	}
}