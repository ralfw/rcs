using System;
using System.Text.RegularExpressions;
using System.Linq;
using CLAP;
using System.Collections.Generic;
using spellchecker.daten.dto;

namespace spellchecker.domain
{
	class Parser {
		public static IEnumerable<Wort> Parse(IEnumerable<string> zeilen) {
			var zeilennummer = 0;
			foreach (var z in zeilen) {
				var wortMatches = System.Text.RegularExpressions.Regex.Matches (z, @"(\w|[-@@äöüÄÖÜß])*");
				foreach (Match w in wortMatches)
					if (w.Value != "")
						yield return new Wort{ Text = w.Value, Zeilennummer = zeilennummer };
				zeilennummer++;
			}
		}
	}
}