using System;
using System.Text.RegularExpressions;
using System.Linq;
using CLAP;
using System.Collections.Generic;
using spellchecker.daten;


namespace spellchecker.provider
{
	class WörterbuchProvider {
		string wörterbuchdateipfad;

		public WörterbuchProvider(string wörterbuchdateipfad) {
			this.wörterbuchdateipfad = wörterbuchdateipfad;
		}

		public Wörterbuch Wörterbuch_laden() {
			var wörter = System.IO.File.ReadAllLines (this.wörterbuchdateipfad);
			return new Wörterbuch (wörter.Select(w => w.Trim()));
		}

		public void Wort_lernen(string wort) {
			System.IO.File.AppendAllText (this.wörterbuchdateipfad, "\n" + wort);
			// mit \n hinzufügen, weil unklar ist, ob die letzte zeile in der wörterbuchdatei mit
			// zeilenende abgeschlossen ist.
			// so werden zwar beim lernen womöglich leerzeilen erzeugt... aber das ist egal. sie werden
			// ja beim laden überlesen.
		}
	}	
}