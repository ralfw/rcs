using System;
using System.Text.RegularExpressions;
using System.Linq;
using CLAP;
using System.Collections.Generic;
using spellchecker.daten;

namespace spellchecker.provider
{
	class TextProvider {
		public Text Text_laden(string textdateipfad) {
			var zeilen = System.IO.File.ReadAllLines (textdateipfad);
			return new Text (zeilen);
		}
	}	
}