﻿using System;
using CLAP;
using System.Collections.Generic;

namespace spellchecker
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			var body = new Body ();
			var head = new Head (body);

			head.Run (args);
		}
	}
}