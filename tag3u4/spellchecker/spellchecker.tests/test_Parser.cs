﻿using NUnit.Framework;
using System;
using System.Linq;
using spellchecker.domain;


namespace spellchecker.tests
{
	[TestFixture ()]
	public class test_Parser
	{
		[Test ()]
		public void TestCase ()
		{
			var worte = Parser.Parse (new[]{ "a, b-c d ", " ef g_h", "@i! äöü. ?ÄÖÜ ß:"});
			Assert.That (worte.Select(w => w.Text), Is.EqualTo (new[]{"a", "b-c", "d", "ef", "g_h", "@i", "äöü", "ÄÖÜ", "ß"}));
			Assert.That (worte.Select(w => w.Zeilennummer), Is.EqualTo(new[]{0,0,0, 1, 1, 2, 2, 2, 2}));
		}
	}
}